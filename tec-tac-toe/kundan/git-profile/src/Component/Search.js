import React from 'react';

const SearchRepo = (props) => {
    return (
        <div className = "Searchbar">
            <input type = "text" type = "text" onChange = {(e) => props.onSearch(e)} placeholder = "Filter Respo.."/>
        </div>
    );
}
export default SearchRepo;