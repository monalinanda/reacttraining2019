import React, { Component } from 'react';
import Board from './Component/Board';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h3 className = "Text-center"> TIC TAC TOE GAME </h3>
        <Board />
      </div>
    );
  }
}

export default App;
