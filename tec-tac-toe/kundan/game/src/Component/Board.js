import React, { Component } from 'react';
import '../global.css';

function Button(props)
{
    return(
        <button className = "Btn">
            {props.btnvalue}
        </button>
    )
}

function DrawBoard(props) {
    return (
        <div className = "Boardrow">   
            <Button btnvalue = {props.rowvalue[0]}/> 
            <Button btnvalue = {props.rowvalue[1]}/>
            <Button btnvalue = {props.rowvalue[2]}/>
        </div>
    )
}

class Board extends Component
{   
    state = [];

    constructor(props)
    {
        super(props);
        this.state = [[0,1,2],[3,4,5],[6,7,8]]
    }
    render(){
        return(
            <div className="Container">
                <DrawBoard rowvalue = {[0,1,2]}/>
                <DrawBoard rowvalue = {[3,4,5]}/>
                <DrawBoard rowvalue = {[6,7,8]}/>
            </div>
        )
    }
} 

export default Board;