import React,{ Component} from 'react';
import FirstCompo111 from './first_1_1_1_compo';
import FirstCompo112 from './first_1_1_2_compo';
class FirstCompo11 extends Component {
    constructor(props) {
    super(props)

    this.state = {
      image:'././image/avtar.png',
      name:'Name Surname',
      nicname:'avatar'
    }
  }
    render(){
        return (
        <div className="FirstCompo11">
               <FirstCompo111 image={this.state.image}/>
                <FirstCompo112 name={this.state.name} nicname={this.state.nicname}/>
        </div>

        );
    }
}

export default FirstCompo11;