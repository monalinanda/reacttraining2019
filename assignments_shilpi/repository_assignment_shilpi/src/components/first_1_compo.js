import React,{ Component} from 'react';
import FirstCompo11 from './first_1_1_compo';
import FirstCompo12 from './first_1_2_compo';
import FirstCompo13 from './first_1_3_compo';
class FirstCompo1 extends Component {
    constructor(props) {
    super(props)

    this.state = {
      followers:6,
      repositories:21,
      following:39
    }
  }
    render(){
        return (
        <div className="FirstCompo1">
                <FirstCompo11 />
                <FirstCompo12 followers={this.state.followers} repositories={this.state.repositories} following={this.state.following}/>
                <FirstCompo13 />
        </div>

        );
    }
}

export default FirstCompo1;